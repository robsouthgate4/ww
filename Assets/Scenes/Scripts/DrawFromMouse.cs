﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawFromMouse : MonoBehaviour {

    public Camera _camera;
    public Shader _drawShader;

    private Material _drawMaterial, _snowMaterial;
    private RenderTexture _splatMap;
    private RaycastHit _hit;


	void Start () {

        _drawMaterial = new Material(_drawShader);
        _drawMaterial.SetColor("_Color", Color.red);
        _snowMaterial = GetComponent<MeshRenderer>().material;

        _splatMap = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGBFloat);
        _snowMaterial.SetTexture("_Splat", _splatMap);

	}
	

	void Update () {

        if(Input.GetKey(KeyCode.Mouse0))
        {
            if(Physics.Raycast( _camera.ScreenPointToRay(Input.mousePosition), out _hit, Mathf.Infinity))
            {
                _drawMaterial.SetVector("_Coordinates", new Vector4(_hit.textureCoord.x, _hit.textureCoord.y, 0, 0));
                RenderTexture temp = RenderTexture.GetTemporary(_splatMap.width, _splatMap.height, 0, RenderTextureFormat.ARGBFloat);
                Graphics.Blit(_splatMap, temp);
                Graphics.Blit(temp, _splatMap, _drawMaterial);
                RenderTexture.ReleaseTemporary(temp);
            }
        }
		
	}

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0,0, 256, 256), _splatMap, ScaleMode.ScaleToFit, false);
    }


}
