﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawWithMouse : MonoBehaviour {

    public Shader _DrawShader;
    public RenderTexture _SplatMap;
    public Camera _Camera;

    private Material _DrawMaterial, _SnowMaterial;
    private RaycastHit _Hit;

	// Use this for initialization
	void Start () {
        _SnowMaterial = GetComponent<MeshRenderer>().material;
        _DrawMaterial = new Material(_DrawShader);
        _DrawMaterial.SetVector("Color", Color.red);
        _SplatMap = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGBFloat);
        _SnowMaterial.SetTexture("_Splat", _SplatMap);
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (Physics.Raycast(_Camera.ScreenPointToRay(Input.mousePosition), out _Hit, Mathf.Infinity))
            {
                _DrawMaterial.SetVector("_Coordinates", new Vector4(_Hit.textureCoord.x, _Hit.textureCoord.y, 0, 0));
                RenderTexture temp = RenderTexture.GetTemporary(_SplatMap.width, _SplatMap.height, 0, RenderTextureFormat.ARGBFloat);
                Graphics.Blit(_SplatMap, temp);
                Graphics.Blit(temp, _SplatMap, _DrawMaterial);
                RenderTexture.ReleaseTemporary(temp);
            }
        }

	}

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(0,0, 256, 256), _SplatMap, ScaleMode.ScaleToFit, false, 1);
    }
}
