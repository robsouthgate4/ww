﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowFollow : MonoBehaviour {

    public Transform objectToFollow;

	void FixedUpdate () {
        transform.position = new Vector3(objectToFollow.position.x, transform.position.y, objectToFollow.position.z);		
	}

}
