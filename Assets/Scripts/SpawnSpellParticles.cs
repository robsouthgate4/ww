﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSpellParticles : MonoBehaviour
{
    List<GameObject> vfx = new List<GameObject>();
    public GameObject firePoint;
    public GameObject effectToSpawn;

    [SerializeField]
    private float fireRate = 1f;
    public void SpawnVfx(Quaternion direction)
    {
        GameObject vfx;

        if (firePoint != null)
        {
            vfx = Instantiate(effectToSpawn, firePoint.transform.position, direction);
        }
    }
}
