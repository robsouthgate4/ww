﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinObject : MonoBehaviour {

    [SerializeField] float xRotationsPerMinute = 1f;
    [SerializeField] float yRotationsPerMinute = 1f;
    [SerializeField] float zRotationsPerMinute = 1f;
	
	
	void Update () {

        // XDegreesPerframe = Time.DeltaTime, 60, 360, xRotationsPerMinute


        // degrees frame^-1, seconds frame^1, seconds minute^-1, degrees rotation^-1, rotation minute^1
        // degrees frame^-1 = seconds frame^1 / seconds minute^-1 * degrees rotation^-1 * rotation minute
        // degrees frame^-1 = frame^1 * degrees


        float XDegreesPerFrame = Time.deltaTime / 60 * 360 * xRotationsPerMinute;
        float YDegreesPerFrame = Time.deltaTime / 60 * 360 * yRotationsPerMinute;
        float ZDegreesPerFrame = Time.deltaTime / 60 * 360 * zRotationsPerMinute;

        //transform.RotateAround(transform.position, transform.right, XDegreesPerFrame);
        //transform.RotateAround(transform.position, transform.up, YDegreesPerFrame);
        transform.RotateAround(transform.position, transform.forward, ZDegreesPerFrame);


    }
}
