﻿Shader "Custom/SnowTracks" {
	Properties {
         _Tess ("Tessellation", Range(1,32)) = 4
		_GroundColor ("Ground Color", Color) = (1,1,1,1)
        _GroundTexture ("Groud Texture", 2D) = "white" {}
		_SnowColor("Snow Color", Color) = (1,1,1,1)
		_SnowTexture("Snow Texture", 2D) = "white" {}
        _Splat ("Splat map)", 2D) = "black" {}
        _Displacement ("Displacement", Range(0, 1.0)) = 0.3
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard addshadow fullforwardshadows vertex:disp tessellate:tessDistance nolightmap

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 4.6
        #include "Tessellation.cginc"

        struct appdata {
            float4 vertex : POSITION;
            float4 tangent : TANGENT;
            float3 normal : NORMAL;
            float2 texcoord : TEXCOORD0;
        };

        float _Tess;

        float4 tessDistance (appdata v0, appdata v1, appdata v2) {
            float minDist = 0.0;
            float maxDist = 100.0;
            return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, minDist, maxDist, _Tess);
        }

        sampler2D _Splat;
        float _Displacement;

        void disp (inout appdata v)
        {
            float d = tex2Dlod(_Splat, float4(v.texcoord.xy,0,0)).r * _Displacement;
            v.vertex.xyz -= v.normal * d;
            v.vertex.xyz += v.normal * _Displacement;
        }

        struct Input {
            float2 uv_SnowTexture;
			float2 uv_GroundTexture;
			float2 uv_Splat;
        };

        sampler2D _SnowTexture;
		sampler2D _GroundTexture;
        sampler2D _NormalMap;
        float _Metallic;
        float _Glossiness;
        float4 _GroundColor;
		float4 _SnowColor;
        
            // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o) {
			float amount = tex2D(_Splat, IN.uv_Splat).r;
            half4 c = lerp(tex2D(_SnowTexture, IN.uv_SnowTexture) * _SnowColor, tex2D(_GroundTexture, IN.uv_GroundTexture) * _GroundColor, amount);
            o.Albedo = c.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
