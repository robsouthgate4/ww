﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovement : MonoBehaviour
{
    [SerializeField]
    float projectileSpeed = 5f;
    Transform transformDirection;

    // Update is called once per frame
    void Update()
    {
        if (projectileSpeed > 0)
        {
            transform.position += (transform.forward * (Time.deltaTime * projectileSpeed));            
        }
        else
        {
            print("No Speed");
        }
        
    }
}
