﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets.Characters.ThirdPerson;

public class Enemy : MonoBehaviour {

    [SerializeField] float maxHealth = 100f;
    [SerializeField] float attackRadius = 10f;
    [SerializeField] float stoppingDistance = 1f;

    float currentHealth = 100f;

    ThirdPersonCharacter _thirdPersonCharacter;
    AICharacterControl _aiCharacterControl;
    Transform _target;

    public float healthAsPercentage {
        get {
            return (currentHealth / maxHealth);
        }
    }

    private void Start()
    {
        _thirdPersonCharacter = GetComponent<ThirdPersonCharacter>();
        _aiCharacterControl = GetComponent<AICharacterControl>();
        _target = _aiCharacterControl.target;
    }

    private void Update()
    {
        float distanceToTarget = Vector3.Distance(_target.position, transform.position);

        if(distanceToTarget <= attackRadius)
        {
            _aiCharacterControl.SetTarget(_target);
        }
        else {
            _aiCharacterControl.SetTarget(transform);
        }

    }


}
