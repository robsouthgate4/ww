﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wand : MonoBehaviour
{
    private float timer;
    private Animator animator;
    private PlayerMovement2 playerMovement;


    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        playerMovement = GetComponent<PlayerMovement2>();
    }

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;

        bool fire = Input.GetKeyDown(KeyCode.Q);
        if (fire)
        {
            animator.SetTrigger("Fire");
            playerMovement.canMove = false;
            timer = 0f;
        }
    }
}
