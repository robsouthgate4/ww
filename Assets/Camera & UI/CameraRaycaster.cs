﻿using UnityEngine;

public class CameraRaycaster : MonoBehaviour
{
    public Layer[] layerPriorities = {
        Layer.Walkable,
        Layer.Enemy,
        Layer.RaycastEndStop
    };

    public LayerMask clickTargetLayerMask;

    [SerializeField] float distanceToBackground = 100f;
    Camera viewCamera;

    RaycastHit currentHit;
    public RaycastHit hit
    {
        get { return currentHit; }
    }

    Layer layerHit;
    public Layer currentLayerhit
    {
        get { return layerHit; }
    }

    public delegate void OnLayerChange(Layer newLayer); // Create delegate type
    public event OnLayerChange onLayerChange; // Create event for delegate

    void Start()
    {
        viewCamera = Camera.main;
    }

    void Update()
    {
        // Look for and return priority layer hit
        foreach (Layer layer in layerPriorities)
        {
            var hit = RaycastForLayer(layer);
            if (hit.HasValue)
            {          

                currentHit = hit.Value;

                if (layerHit != layer)
                {
                    layerHit = layer;
                    onLayerChange(layer); //Update delegate event
                }                
                
                return;
            }
        }
   
        // Otherwise return background hit
        currentHit.distance = distanceToBackground;
        layerHit = Layer.RaycastEndStop;
    }

    RaycastHit? RaycastForLayer(Layer layer)
    {

        int layerMask = 1 << (int)layer; // See Unity docs for mask formation
        Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit; // used as an out parameter
        bool hasHit = Physics.Raycast(ray, out hit, distanceToBackground, layerMask);
        if (hasHit)
        {
            return hit;
        }
        return null;
    }
}
