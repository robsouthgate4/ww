﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CameraRaycaster))]
public class CursorAffordance : MonoBehaviour {

    [SerializeField] Texture2D walkCursor = null;
    [SerializeField] Texture2D attackCursor = null;
    [SerializeField] Texture2D errorCursor = null;

    Texture2D currentCursor;
    CameraRaycaster cameraRaycaster;
    [SerializeField] Vector2 hotspot;

	
	void Start () {
        hotspot = new Vector2(0, 0);
        cameraRaycaster = GetComponent<CameraRaycaster>();
        cameraRaycaster.onLayerChange += HandleLayerChange;
	}
	
	void HandleLayerChange(Layer newLayer) { // Regiser new handler for delegate update
        print("Cursor is over new layer");
        switch (newLayer)
        {
            case Layer.Walkable:
                currentCursor = walkCursor;
                break;
            case Layer.Enemy:
                currentCursor = attackCursor;
                break;
            case Layer.RaycastEndStop:
                currentCursor = errorCursor;
                break;
            default:
                currentCursor = errorCursor;
                break;
        }
       Cursor.SetCursor(currentCursor, hotspot, CursorMode.Auto);
	}
}
