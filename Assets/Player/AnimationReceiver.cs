﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationReceiver : MonoBehaviour {

    private PlayerMovement2 playerMovement;

    void Awake()
    {
        playerMovement = GetComponentInParent<PlayerMovement2>();
    }

    public void CanMove(float val)
    {
        playerMovement.canMove = true;
        print("Can move!");
    }
}
