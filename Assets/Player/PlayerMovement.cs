﻿using UnityEngine;

using UnityStandardAssets.Characters.ThirdPerson;

[RequireComponent(typeof(ThirdPersonCharacter))]
public class PlayerMovement : MonoBehaviour
{
    ThirdPersonCharacter thirdPersonCharacter;   // A reference to the ThirdPersonCharacter on the object
    CameraRaycaster cameraRaycaster;
    Vector3 currentMovement, clickPoint, currentDestination;

    bool isInDirectMode;

    public float walkMoveRadius = 0.25f;
    public float attackMoveRadius = 5.0f;

    private void Start()
    {
        cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
        thirdPersonCharacter = GetComponent<ThirdPersonCharacter>();
        currentDestination = transform.position;
    }

    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {

        if (Input.GetKeyDown(KeyCode.G))
        {
            isInDirectMode = !isInDirectMode;
            currentDestination = transform.position;
        }

        if (isInDirectMode)
        {
            // Process direct movement
            ProcessDirectMovement();
        }
        else
        {
            ProcessMouseMovement();            
        }
       
    }

    private void ProcessDirectMovement()
    {
        var h = Input.GetAxis("Horizontal");
        var v = Input.GetAxis("Vertical");

        Debug.Log(v);

        Vector3 camForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 movement = v * camForward + h * Camera.main.transform.right;

        thirdPersonCharacter.Move(movement, false, false);
    }

    private void ProcessMouseMovement()
    {
        if (Input.GetMouseButton(0))
        {
            clickPoint = cameraRaycaster.hit.point;
            switch (cameraRaycaster.currentLayerhit)
            {
                case Layer.Walkable:
                    currentDestination = ShortenedDistance(clickPoint, walkMoveRadius);  // So not set in default case                   
                    break;
                case Layer.Enemy:
                    currentDestination = ShortenedDistance(clickPoint, attackMoveRadius);  // So not set in default case   
                    break;
                default:
                    print("Unexpected click area");
                    return;
            }
        }

        WalkToPosition();
       
    }

    private void WalkToPosition()
    {
        Vector3 playerToClickPoint = currentDestination - transform.position;

        if (playerToClickPoint.magnitude >= walkMoveRadius)
        {
            thirdPersonCharacter.Move(playerToClickPoint, false, false);
        }
        else
        {
            thirdPersonCharacter.Move(Vector3.zero, false, false);
        }
    }

    Vector3 ShortenedDistance(Vector3 destination, float shortAmount)
    {
        Vector3 reductionVector = (destination - transform.position).normalized * shortAmount;
        return destination - reductionVector;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawLine(transform.position, currentDestination);
        Gizmos.DrawSphere(currentDestination, 0.1f);
        Gizmos.DrawSphere(clickPoint, 0.08f);

        Gizmos.color = new Color(255f, 0f, 0f, 0.5f);
        Gizmos.DrawWireSphere(transform.position, attackMoveRadius);
    }
}
