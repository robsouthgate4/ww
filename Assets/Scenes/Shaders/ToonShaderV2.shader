﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/ToonShaderV2" {
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _ToonLut ("Toon LUT Texture", 2D) = "white" {}
        _RimColor("Rim Color", COLOR) = (1,1,1,1)
        _RimPower("Rim Power", Float) = 1
        _Color("Color", COLOR) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            
            Tags
			{
				"LightMode" = "ForwardBase"
			}
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            #pragma multi_compile_fwdbase
            
            #include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;               
                float2 uv : TEXCOORD0;                                
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
                float3 viewDir: TEXCOORD2;
                UNITY_FOG_COORDS(1)                                
            };

            sampler2D _MainTex;
            sampler2D _ToonLut;
            float4 _Color;
            float3 _RimColor;
            float _RimPower;
            float4 _MainTex_ST;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.viewDir = normalize(UnityWorldSpaceViewDir(mul(unity_ObjectToWorld, v.vertex)));
                o.normal = UnityObjectToWorldNormal(v.normal);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                float3 normal = normalize(i.normal);
                float ndotl = dot(normal, _WorldSpaceLightPos0);
                float ndotv = saturate(dot(normal, i.viewDir));
                
                
                fixed4 toonLut = tex2D(_ToonLut, i.uv);
                float3 rim = _RimColor * pow(1 - ndotv, _RimPower) * ndotl;
                
                float3 directDiffuse = toonLut * _LightColor0;
                float3 indirectDiffuse = unity_AmbientSky;
                
                
                fixed4 col = tex2D(_MainTex, i.uv) * _Color;
                /*col.rgb *= directDiffuse + indirectDiffuse;                
                col.rgb += rim;
                col.a = 1.0;*/
                
                
                // apply fog
                //UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}