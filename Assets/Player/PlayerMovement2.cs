﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement2 : MonoBehaviour
{
    private Animator animator;
    private CharacterController characterController;
    private float horizontal;
    private float vertical;
    private Vector3 movement;
    [SerializeField]
    private float moveSpeed = 100f;
    [SerializeField]
    private float turnSpeed = 10f;
    private float timer;
    private float fireRate = 0.3f;
    private SpawnSpellParticles spawnSpellParticles;

    private  Quaternion newDirection;
    public bool canMove;

    public Quaternion Rotation
    {
        get
        {
            return newDirection;
        }
    }

    void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        animator.fireEvents = true;
        characterController = GetComponent<CharacterController>();
        spawnSpellParticles = GetComponent<SpawnSpellParticles>();

        canMove = true; // Variable updated in child components Animation event
    }

    // Animation event callback
    public void SpellCast(float val)
    {
        canMove = true;
    }

    // Animation event callback
    public void SpellBeingCast()
    {
        Quaternion direction = Quaternion.LookRotation(transform.forward);
        spawnSpellParticles.SpawnVfx(direction);
    }

    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        // If attack animation event is complete, allow character to carry on moving
        if (canMove)
        {
            ApplyMovement();
        }        

        // Onlt apply roation if character is moving, to avoid snapping
        if (movement.magnitude > 0)
        {
            ApplyRotation();
        }

    }
    
    // Ronseal
    private void ApplyMovement()
    {
        movement = new Vector3(horizontal, 0, vertical);
        characterController.Move(movement * (Time.deltaTime * moveSpeed));
        animator.SetFloat("BlendY", movement.magnitude);
        //animator.SetFloat("BlendY", vertical);
    }

    // Ronseal
    private void ApplyRotation()
    {
        newDirection = Quaternion.LookRotation(movement);
        transform.rotation = Quaternion.Slerp(transform.rotation, newDirection, turnSpeed * Time.deltaTime);
    }

}
